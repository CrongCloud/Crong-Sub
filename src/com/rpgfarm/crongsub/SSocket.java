package com.rpgfarm.crongsub;

import javax.annotation.Nullable;

import org.bukkit.event.Event;

import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;

public class SSocket extends Effect {
	private Expression<String> Text;
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean paramKleenean, ParseResult paramParseResult) {
		Text = (Expression<String>) expr[0];
		return true;
	}

	@Override
	public String toString(@Nullable Event e, boolean b) {
		return "send socket packet";
	}

	@Override
	protected void execute(Event e) {
    	main.ClientRun(Text.getSingle(e));
	}
}